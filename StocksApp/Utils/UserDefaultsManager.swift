//
//  UserDefaultsManager.swift
//  StocksApp
//
//  Created by  Amber  on 11/1/21.
//

import Foundation

final class UserDefaultsManager {
    private static let symbolKey = "SYMBOL_KEY"
    private static let timerKey = "TIMER_KEY"
    var savedSymbols = [String]()
    @Published var timerMin: Int? = nil
    static let shared = UserDefaultsManager()
    private init() {
        get()
    }
    func get() {
        if let saved = UserDefaults.standard.array(forKey: Self.symbolKey) as? [String] {
            savedSymbols = saved
        }
    }
    func getTiming() {
        if let timing = UserDefaults.standard.integer(forKey: Self.timerKey) as? Int{
            timerMin = timing
        }
    }
    func setTiming(timing: Int) {
        UserDefaults.standard.set(timing, forKey: Self.timerKey)
    }
    
    func set(symbol: String) {
        savedSymbols.append(symbol)
        UserDefaults.standard.set(self.savedSymbols, forKey: Self.symbolKey)
    }
}

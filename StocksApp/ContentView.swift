//
//  ContentView.swift
//  StocksApp
//
//  Created by  Amber  on 10/29/21.
//

import SwiftUI

class GlobalEnvironement: ObservableObject {
    @Published var timerMin = UserDefaultsManager.shared.timerMin
}

struct ContentView: View {
    
    @ObservedObject var stockManager = StockQuoteManager()
    @ObservedObject var newsManager = NewsDownloadManager()
    @State private var stocks = UserDefaultsManager.shared.savedSymbols
    @State private var searchTerm = ""
    @State private var newsOpen = false
    @State private var oldStocks = [String]()
    @State private var downloading = false
    @State private var timer: Timer? = nil
    @EnvironmentObject var env: GlobalEnvironement
    
    
    init() {
        UITableView.appearance().separatorStyle = .none
        UITableView.appearance().backgroundColor = .clear
        UITableViewCell.appearance().backgroundColor = .clear
        UserDefaultsManager.shared.getTiming()
        print(UserDefaultsManager.shared.timerMin)
       
    }
    
    var body: some View {
        ZStack {
            Color.black
            VStack(alignment: .leading) {
                if newsOpen {
                    withAnimation {
                        MiniQuoteView(stockQuotes: stockManager)
                            .foregroundColor(.white)
                            .padding(.top, 50)
                            .frame(height: newsOpen ? 100 : 0)
                            .transition(.move(edge: .top))
                    }
                } else {
                    withAnimation {
                        HeaderView(stocks: $stocks, refreshTiming: String(UserDefaultsManager.shared.timerMin ?? 5))
                            .padding(.top, 50)
                            .frame(height: newsOpen ? 0 : 100)
                            .transition(.move(edge: .top))
                    }
                }
                
                List {
                    Group {
                        SearchTextView(searchTerm: $searchTerm)
                        
                        if downloading{
                            HStack{
                                Spacer()
                                ProgressView("Loading").progressViewStyle(CircularProgressViewStyle(tint: .purple))
                                Spacer()
                            }
                            
                        }
                        
                        ForEach(getQuotes()) { quote in
                            QuoteCell(quote: quote)
                        }
                    }.listRowBackground(Color.clear).listRowInsets(EdgeInsets())
                }.onAppear {
                    fetchData(for: stocks)
                    oldStocks = stocks
                }.onChange(of: stocks, perform: { value in
                    fetchData(for: stocks.difference(from: oldStocks))
                    oldStocks = stocks
                })
                    .onReceive(Timer.publish(every: Double(env.timerMin ?? 1) * 6.0, on: .main, in: .default).autoconnect(), perform: {(_) in
                        print("TIME \(env.timerMin)")
                        refreshData(for: stocks)
                    })
                    .listStyle(PlainListStyle())
                    .foregroundColor(.white)
            }.padding(.horizontal, 32)
            .padding(.bottom, UIScreen.main.bounds.height * 0.21)
            
            NewsSheetView(newsOpen: $newsOpen, newsManager: newsManager)
        }.edgesIgnoringSafeArea(.all)
    }
    
    private func getQuotes() -> [Quote] {
        return searchTerm.isEmpty ? stockManager.quotes : stockManager.quotes.filter { $0.symbol.lowercased().contains(searchTerm.lowercased()) }
    }

    private func refreshData(for symbols: [String]) {
        self.downloading = true
        stockManager.quotes = []
        stockManager.download(stocks: symbols) { _ in
            self.downloading = false
        }
    }
    
    private func fetchData(for symbols: [String]) {
        self.downloading = true
        stockManager.download(stocks: symbols) { _ in
            self.downloading = false
        }
    }
    
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView().environmentObject(GlobalEnvironement())
    }
}

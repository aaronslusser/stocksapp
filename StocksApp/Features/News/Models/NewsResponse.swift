//
//  NewsResponse.swift
//  StocksApp
//
//  Created by  Amber  on 10/29/21.
//

import Foundation

struct NewsResponse: Codable {
    var articles: [News]
    
}

//
//  SearchResponse.swift
//  StocksApp
//
//  Created by  Amber  on 11/1/21.
//

import Foundation

struct SearchResponse: Codable {
    var bestMatches: [Search]
}

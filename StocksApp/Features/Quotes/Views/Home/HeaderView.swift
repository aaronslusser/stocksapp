//
//  HeaderView.swift
//  StocksApp
//
//  Created by  Amber  on 10/29/21.
//

import SwiftUI

struct HeaderView: View {
    
    private let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM dd"
        return formatter
    }()
    
    @Binding var stocks: [String]
    @State var refreshTiming: String
    @State private var showTiming = false
    @State private var showSearch = false
    @EnvironmentObject var env: GlobalEnvironement
    
    private func setTiming(timingMin: Int){
        UserDefaultsManager.shared.setTiming(timing: timingMin)
        self.env.timerMin = timingMin
        refreshTiming = String(timingMin)
    }
    
    var body: some View {
        HStack{
            VStack(alignment: .leading, spacing: -5) {
                Text("Stocks")
                    .foregroundColor(.white)
                    .bold()
                Text("\(Date(), formatter: dateFormatter)")
                    .foregroundColor(.gray)
                    .bold()
            }.font(.title)
            Spacer()
            VStack(spacing: -5){
                Button {
                    showTiming.toggle()
                } label: {
                    Image(systemName: "clock.fill")
                        .foregroundColor(.white)
                        .frame(width: 40, height: 40)
                }
                .alert("Change Refresh Rate", isPresented: $showTiming) {
                    Button("1 min") { setTiming(timingMin: 1) }
                    Button("5 min") { setTiming(timingMin: 5) }
                    Button("10 min") { setTiming(timingMin: 10) }
                    Button("30 min") { setTiming(timingMin: 30) }
                    Button("Cancel", role: .cancel) { }
                }
                Text("\(refreshTiming) min").foregroundColor(.white)
            }
            
            
            
            Button {
                showSearch.toggle()
            } label: {
                Image(systemName: "plus")
                    .font(.title)
                    .foregroundColor(.white)
            }.sheet(isPresented: $showSearch, onDismiss:  {
                self.stocks = UserDefaultsManager.shared.savedSymbols
            }, content: {
                SearchView()
            })

        }
    }
}

struct HeaderView_Previews: PreviewProvider {
    static var previews: some View {
        HeaderView(stocks: .constant(["AAPL", "GOOG"]), refreshTiming: "5").background(Color.black)
    }
}

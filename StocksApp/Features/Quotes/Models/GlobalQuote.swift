//
//  GlobalQuote.swift
//  StocksApp
//
//  Created by  Amber  on 10/29/21.
//

import Foundation

struct GlobalQuoteResponse: Codable {
    var quote: Quote
    
    private enum CodingKeys: String, CodingKey {
        case quote = "Global Quote"
    }
}


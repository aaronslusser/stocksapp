//
//  QuoteManagerProtocol.swift
//  StocksApp
//
//  Created by  Amber  on 10/29/21.
//

import Foundation

protocol QuoteManagerProtocol {
    var quotes: [Quote] { get set }
    func download(stocks: [String], completion: @escaping (Result<[Quote], NetworkError>) -> Void)
}

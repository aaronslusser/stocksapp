//
//  StocksAppApp.swift
//  StocksApp
//
//  Created by  Amber  on 10/29/21.
//

import SwiftUI

@main
struct StocksAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView().environmentObject(GlobalEnvironement())
        }
    }
}
